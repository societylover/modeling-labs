﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        float T1, T2, M1, M2, Tsredi;
        int  TimeD;


        public Form1()
        {
            InitializeComponent();
        }

        void getv()
        {
            T1 = (float)Convert.ToDouble(textBox1.Text);
            M1 = (float)Convert.ToDouble(textBox2.Text);
            T2 = (float)Convert.ToDouble(textBox3.Text);
            M2 = (float)Convert.ToDouble(textBox4.Text);
            Tsredi = (float)Convert.ToDouble(textBox7.Text);
            TimeD = Convert.ToInt32(textBox6.Text);
        }

        float temperature (float T1,float T2, float M1, float M2){
            return ((T1*M1 + T2*M2)/(M1+M2));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            getv();
            float temper = temperature(T1,T2,M1,M2);
            label11.Text = "Температура смеси (0 м.)= " + Convert.ToString(temper) + "\n";           
            double time = 0;
            float dt = 0.6f;
            for (int i = 0; i < TimeD; i++)
            {
                //dt = T1 - Tsredi;
                T1 -= (float)(dt * 0.1);
                //dt = temper - Tsredi;
                temper -= (float)(dt * 0.1);          
            }
            label12.Text = "Температура смеси после " + time + " м. " + " = " + temper + "\n";
            temper = temperature(T1, T2, M1, M2);
            label13.Text = "Температура смеси после " + time + " м. " + " = " + temper + "\n";
        }
    }
}
