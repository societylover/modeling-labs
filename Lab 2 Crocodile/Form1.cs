﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace croco
{
    public partial class Form1 : Form
    {
        struct Point
        {
            public int x;
            public int y;
            public int forward;
        }
        int halfW, halfH;
        List<Point> way = new List<Point>();
        int forwardchance;
        public Form1()
        {
            InitializeComponent();
        }


        void generate()
        {
            Random rnd = new Random();
            for (int i = 0; i < 1000; i++)
            {
                Point t = new Point();
                t.x = halfW;
                t.y = halfH;
                
                t.forward = rnd.Next(0, 4);
                switch (t.forward)
                {
                    case 0: t.y -= 10; 
                    break;
                    case 1: t.x -= 10;
                    break;
                    case 2: t.y += 10;
                    break;
                    case 3: t.x += 10;
                    break;
                }
                int newF = 1;
                switch (forwardchance)
                {
                    case 33:
                        newF = rnd.Next(0, 3);
                        break;
                    case 50: newF = rnd.Next(0, 4);
                        break;
                }
                
                for (int j = 0; j < 400; j++)
                {
                    if (forwardchance == 33)
                    {
                        switch (t.forward)
                        {
                            case 0:
                                {
                                    switch (newF)
                                    {
                                        case 0:
                                            {
                                                t.x += 10;
                                                t.forward = 3;
                                            }
                                            break;
                                        case 1:
                                            {
                                                t.y -= 10;
                                                t.forward = 0;
                                            }
                                            break;
                                        case 2:
                                            {
                                                t.x -= 10;
                                                t.forward = 2;
                                            }
                                            break;
                                    }
                                }
                                break;
                            case 1:
                                {
                                    switch (newF)
                                    {
                                        case 0:
                                            {
                                                t.y -= 10;
                                                t.forward = 0;
                                            }
                                            break;
                                        case 1:
                                            {
                                                t.x -= 10;
                                                t.forward = 1;
                                            }
                                            break;
                                        case 2:
                                            {
                                                t.y += 10;
                                                t.forward = 2;
                                            }
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                {
                                    switch (newF)
                                    {
                                        case 0:
                                            {
                                                t.x -= 10;
                                                t.forward = 1;
                                            }
                                            break;
                                        case 1:
                                            {
                                                t.y += 10;
                                                t.forward = 2;
                                            }
                                            break;
                                        case 2:
                                            {
                                                t.x += 10;
                                                t.forward = 3;
                                            }
                                            break;
                                    }
                                }
                                break;
                            default:
                                {
                                    switch (newF)
                                    {
                                        case 0:
                                            {
                                                t.y += 10;
                                                t.forward = 2;
                                            }
                                            break;
                                        case 1:
                                            {
                                                t.x += 10;
                                                t.forward = 3;
                                            }
                                            break;
                                        case 2:
                                            {
                                                t.y -= 10;
                                                t.forward = 0;
                                            }
                                            break;
                                    }
                                }
                                break;

                        }
                    }
                    else if (forwardchance == 50)
                    {
                            switch (t.forward)
                            {
                                case 0:
                                    {
                                        switch (newF)
                                        {
                                            case 0: 
                                                {
                                                    t.x += 10;
                                                    t.forward = 3;
                                                }
                                                break;
                                            case 1: case 2:
                                                {
                                                    t.y -= 10;
                                                    t.forward = 0;
                                                }
                                                break;
                                            case 3:
                                                {
                                                    t.x -= 10;
                                                    t.forward = 2;
                                                }
                                                break;
                                        }
                                    }
                                    break;
                                case 1:
                                    {
                                        switch (newF)
                                        {
                                            case 0:
                                                {
                                                    t.y -= 10;
                                                    t.forward = 0;
                                                }
                                                break;
                                            case 1: case 2:
                                                {
                                                    t.x -= 10;
                                                    t.forward = 1;
                                                }
                                                break;
                                            case 3:
                                                {
                                                    t.y += 10;
                                                    t.forward = 2;
                                                }
                                                break;
                                        }
                                    }
                                    break;
                                case 2:
                                    {
                                        switch (newF)
                                        {
                                            case 0:
                                                {
                                                    t.x -= 10;
                                                    t.forward = 1;
                                                }
                                                break;
                                            case 1: case 2:
                                            {
                                                    t.y += 10;
                                                    t.forward = 2;
                                                }
                                                break;
                                            case 3:
                                                {
                                                    t.x += 10;
                                                    t.forward = 3;
                                                }
                                                break;
                                        }
                                    }
                                    break;
                                default:
                                    {
                                        switch (newF)
                                        {
                                            case 0:
                                                {
                                                    t.y += 10;
                                                    t.forward = 2;
                                                }
                                                break;
                                            case 1: case 2:
                                                {
                                                    t.x += 10;
                                                    t.forward = 3;
                                                }
                                                break;
                                            case 3:
                                                {
                                                    t.y -= 10;
                                                    t.forward = 0;
                                                }
                                                break;
                                        }
                                    }
                                    break;
                            }
                    }
                    switch (forwardchance)
                    {
                        case 33: newF = rnd.Next(0, 3);
                            break;
                        case 50: newF = rnd.Next(0, 4);
                            break;
                    }
                    
                }
                way.Add(t);
                comboBox1.Items.Add("Квартал : x - "+Convert.ToString(t.x / 10)+" y -"+Convert.ToString(t.y / 10));
            }
        }

        void draw()
        {
            Graphics gra = pictureBox1.CreateGraphics();
            Brush bra = new SolidBrush(Color.YellowGreen);
            Font drawFont = new Font("Century Gothic", 5, FontStyle.Bold);
            Pen pen = new Pen(Color.Gray);
            pen.DashStyle = DashStyle.Dash;
            for (int i = 0; i < pictureBox1.Width; i += 10)
            {
                gra.DrawLine(pen,i, 0, i , pictureBox1.Width);
                gra.DrawString(Convert.ToString(i / 10), drawFont,Brushes.Black,i,0);
                gra.DrawLine(pen, 0, i, pictureBox1.Height, i);
                gra.DrawString(Convert.ToString(i / 10), drawFont, Brushes.Black, 0, i);
            }
            foreach (Point t in way)
            {
                gra.FillRectangle(bra, t.x+1, t.y+1, 9, 9);
            }
            gra.FillRectangle(Brushes.Crimson, halfH+1, halfW+1, 9, 9);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void highlight(object sender, EventArgs e)
        {
            int index = comboBox1.SelectedIndex;
            Graphics gra = pictureBox1.CreateGraphics();
            Brush bra = new SolidBrush(Color.Blue);
            gra.FillRectangle(bra, way[index].x + 1, way[index].y + 1, 9, 9);
            for (double i = 0; i < 10; i += 0.001)
            {
                for (double j = 0; j < 30; j += 0.001) { }   
            }
            gra.FillRectangle(Brushes.YellowGreen, way[index].x + 1, way[index].y + 1, 9, 9);
        }

        bool findin(ref List <Point> inP, Point what)
        {
            foreach (Point t in inP)
            {
                if (t.x == what.x && t.y == what.y) return false;
            }
            return true;
        }

        void analysis()
        {
            string result = "";
            double r = 0, h = 0;
            result += "Вероятность нахождения в R:\n";
            result += " 1 км: \t";
            double count = 0;
            List<Point> km1 = new List<Point>(); 
            foreach (Point t in way)
            {
                r = pictureBox1.Width / 90 + halfH;
                h = Math.Sqrt(t.x * t.x + t.y * t.y);
                if (h <= r)
                {
                    count++;
                    km1.Add(t);
                }
            }
            try { result += Convert.ToString(count/10); } 
            catch { result += "0"; }
            result += "%\n 5 км: \t"; 
            count = 0;
            List<Point> km5 = new List<Point>();
            foreach (Point t in way)
            {
                r = pictureBox1.Width / 18 + halfH;
                h = Math.Sqrt(t.x * t.x + t.y * t.y);
                if (h <= r && findin(ref km1, t)) { count++; km5.Add(t); }
            }
            try { result += Convert.ToString(count / 10); }
            catch { result += "0"; }
            result += "%\n 10 км: \t";
            List<Point> km10 = new List<Point>();
            count = 0;
            foreach (Point t in way)
            {
                r = pictureBox1.Width / 8 + halfH;
                h = Math.Sqrt(t.x * t.x + t.y * t.y);
                if (h <= r && findin(ref km1, t) && findin(ref km5, t)) { count++; km10.Add(t);
                }
            }
            try { result += Convert.ToString(count / 10); }
            catch { result += "0"; }
            result += "%\n 20 км: \t";
            count = 0;
            List<Point> km20 = new List<Point>();
            foreach (Point t in way)
            {
                r = pictureBox1.Width / 4 + halfH;
                h = Math.Sqrt(t.x * t.x + t.y * t.y);
                if (h <= r && findin(ref km1, t) && findin(ref km5, t) && findin(ref km10, t)) { count++; km20.Add(t); }
            }
            try { result += Convert.ToString(count / 10); }
            catch { result += "0"; }
            result += "%\n 40 км: \t";
            count = 0;
            List<Point> km40 = new List<Point>();
            foreach (Point t in way)
            {
                r = pictureBox1.Width / 2 + halfH;
                h = Math.Sqrt(t.x * t.x + t.y * t.y);
                if (h <= r && findin(ref km1, t) && findin(ref km5, t) && findin(ref km10, t) && findin(ref km20, t)) { count++; km40.Add(t); }
            }
            try { result += Convert.ToString(count / 10); }
            catch { result += "0"; }
            result += "%\n 70 км: \t";
            count = 0;
            List<Point> km70 = new List<Point>();
            foreach (Point t in way)
            {
                r = pictureBox1.Width / 1.1 + halfH;
                h = Math.Sqrt(t.x * t.x + t.y * t.y);
                if (h <= r && findin(ref km1, t) && findin(ref km5, t) && findin(ref km10, t) && findin(ref km20, t) && findin(ref km40, t)) { count++; km70.Add(t); }
            }
            try { result += Convert.ToString(count / 10); }
            catch { result += "0"; }
            result += "%";
            label8.Text = result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label8.Text = " ";
            comboBox1.Items.Clear();
            if (radioButton1.Checked) forwardchance = 33;
            if (radioButton2.Checked) forwardchance = 50;
            halfH = pictureBox1.Height / 2;
            halfW = pictureBox1.Width / 2;
            way.Clear();
            generate();
            pictureBox1.Refresh();
            draw();
            analysis();
        }
    }
}
